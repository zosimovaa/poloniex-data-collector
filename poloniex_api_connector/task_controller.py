import time
import json
import logging
import traceback
from queue import Queue, Full, Empty
from.task import TaskEmitter

# logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TaskControllerError(Exception):
    """Базовое исключение Task controller"""
    pass


def handle_task_exception(func):
    """Обработчик исключений для класса TaskController"""
    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)

        except Full:
            # Внешняя ошиюка - переполнение очереди.
            logger.error("The tasks queue is full! Ticker task didn't added - {0}".format(args))
            raise Full

        except Exception as e:
            # Внешняя ошибка - неожиданное исключение.
            logger.error("Some error occurred - {}".format(e))
            logger.error(traceback.format_exc())
            raise TaskControllerError(e)
        return result
    return wrapper


class TaskController(TaskEmitter):
    MAX_QUEUE_SIZE = 1000       # Максимальный размер очыереди
    MAX_ATTEMPTS = 3            # Максимальное количество попыток отправки задания
    TASK_LIFETIME = 10          # Длительность жизни активного задания

    def __init__(self, queue):
        super().__init__()
        self.queue = queue
        self.active_task = None
        logger.debug("Task controller initialized")

    def task_control(self, payload):
        """Основной контроллер, вызываемый при обработке входящего сообщения"""
        tid, sequence_num, messages = self.__handle_message(payload)
        self.__task_process()
        return tid, sequence_num, messages

    def __handle_message(self, message):
        """Обработка входящего сообщения для обновления статуса текущего задания"""
        payload = json.loads(message)
        if 'error' in payload:
            # Обработка ошибки - {'error': 'Invalid channel.'} {'error': 'Invalid command.'}
            logger.error("Incoming message error {}".format(json.dumps(payload)))
            self.__drop_task(reason=payload['error'])
            tid, sequence_num, messages = None, None, None
        else:
            payload.extend([None, None])
            tid, sequence_num, messages = payload[:3]

            if self.active_task is not None and tid == self.active_task["tid"]:
                if self.active_task["cmd"] == self.SUBSCRIBE:
                    self.__check_result_subscribe(sequence_num, messages)
                elif self.active_task["cmd"] == self.UNSUBSCRIBE:
                    self.__check_result_unsubscribe(sequence_num)
                else:
                    # unknown command
                    self.__drop_task(reason=payload['unknown command'])

        return tid, sequence_num, messages

    @handle_task_exception
    def __task_process(self):
        """Постановка следующего задания в очередь"""
        if self.active_task is None:
            try:
                self.active_task = self.queue.get_nowait()
                self.active_task["ts"] = time.time()
                self.active_task["attempt"] += 1

                message = json.dumps({
                    "command": self.active_task["cmd"],
                    "channel": int(self.active_task["tid"])
                })
                if self.active_task["attempt"] <= self.MAX_ATTEMPTS:
                    # todo проработать логику ошибок при отправке
                    self.send_command(message)
                else:
                    self.__drop_task(reason="Max attempts reached")

            except Empty:
                pass
        else:
            if time.time() - self.active_task["ts"] > self.TASK_LIFETIME:
                logger.warning("Task timeout. Push to the end of queue: {0}".format(self.active_task))
                self.put_task(self.active_task)
                self.active_task = None

    def send_command(self, command):
        print(command)

    @handle_task_exception
    def __check_result_subscribe(self, sequence_num, messages):
        """Логика проверки успешного исполнения заданий подписки (subscribe)"""
        logger.debug("__check_result_subscribe method called with sequence {0} and messages {1}"
                     .format(sequence_num, messages))
        if sequence_num == 2:
            # 2 приходит если мы уже подписаны на пару
            self.unsubscribe(tid=self.active_task["tid"])
            self.subscribe(tid=self.active_task["tid"])
            self.__drop_task(reason="resubscribe")
        else:
            for message in messages:
                content_type = ord(message[0])
                if content_type == 105:
                    self.__task_done()
                    break

    @handle_task_exception
    def __check_result_unsubscribe(self, sequence_num):
        logger.debug("__check_result_unsubscribe method called with sequence {0}".format(sequence_num))
        """Логика проверки успешного исполнения заданий отписки (unsubscribe)"""
        if sequence_num == 0:
            # 0 приходит при успешном выполнении задания отписки
            self.__task_done()

    # @handle_task_exception
    # def add_task(self, cmd, tid):
    #    logger.debug("add_task method called with cmd {0} and tid {1}".format(cmd, tid))
    #    task = {"tid": tid, "cmd": cmd,  "attempt": 0}
    #    self.queue.put_nowait(task)

    def __task_done(self):
        """Пометка задачи как 'выполненная'"""
        logger.debug("Task done: {}".format(self.active_task))
        self.active_task = None

    def __drop_task(self, reason=None):
        """Удаление проблемной задачи из очереди"""
        logger.warning("Task dropped with error ({1}): {0}".format(self.active_task, reason))
        self.active_task = None
