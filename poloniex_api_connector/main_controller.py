import ssl
import time
from datetime import datetime
import logging
import traceback
from threading import Thread, RLock

from queue import Queue, Full, Empty

from .ticker import PairNotInitialized
from .task_controller import TaskController
from .ticker_controller import TickerController
from .stat_module import StatModule


from websocket import WebSocketApp

logger = logging.getLogger(__name__)


class PoloniexError(Exception):
    """+Базовый класс исключения для API"""
    def __str__(self):
        return "Basic poloniex exception"


class WssConnectionError(PoloniexError):
    """+Исключение при потере соединения WSS"""
    def __str__(self):
        return "Wss error"


class HeartbeatLostError(Exception):
    """+Исключение при отсутствии обновлений от poloniex"""

    def __init__(self, ts_current, ts_heartbeat):
        self.ts_current = datetime.utcfromtimestamp(ts_current).strftime('%Y-%m-%d %H:%M:%S')
        self.ts_heartbeat = datetime.utcfromtimestamp(ts_heartbeat).strftime('%Y-%m-%d %H:%M:%S')
        self.timeout = ts_current - ts_heartbeat

    def __str__(self):
        message = "Heartbeat lost. Timeout: {0:.2f}".format(self.timeout)
        message += ". Raised at {}".format(self.ts_current)
        message += ". Last data at {}".format(self.ts_heartbeat)
        return message

class TasksQueueFullError(PoloniexError):
    """Исключение при переполнении очереди заданий"""
    pass


def handle_api_exception(func):
    """Обработчик исключений для класса PoloniexWebSocketAPI"""
    def wrapper(websocket_api, *args, **kwargs):
        try:
            result = func(websocket_api, *args, **kwargs)

        except Exception as e:
            if websocket_api.error is None:
                websocket_api.error = e
            logger.error(e)
            logger.error(traceback.format_exc())
            raise e

        return result
    return wrapper


def check_state(func):
    """Функция проверяет активность соединения websocket по таймауту обновления"""
    def wrapper(websocket_api, *args, **kwargs):
        with websocket_api.lock:
            current_ts = time.time()
            timeout = current_ts - websocket_api.heartbeat

        if timeout > websocket_api.CONNECTION_TIMEOUT:
            raise HeartbeatLostError(current_ts, websocket_api.heartbeat)
        elif websocket_api.queue.full():
            raise Full
        else:
            result = func(websocket_api, *args, **kwargs)
        return result
    return wrapper


def check_error(func):
    """Функция проверяет наличие активной ошибки по соединению websocket и выбрасывает исключение во внешний процесс"""
    def wrapper(websocket_api, *args, **kwargs):
        if websocket_api.error is not None:
            raise websocket_api.error()
        else:
            result = func(websocket_api, *args, **kwargs)
        return result
    return wrapper


class PoloniexWebSocketAPI(TaskController, TickerController):
    """Класс реализует работу с websocket api poloniex.com"""
    WEB_SOCKET_URL = "wss://api2.poloniex.com"
    CONNECTION_TIMEOUT = 10
    WARM_UP_TIME = 30

    def __init__(self):
        self.queue = Queue(self.MAX_QUEUE_SIZE)

        TaskController.__init__(self, self.queue)
        TickerController.__init__(self, self.queue)
        self.error = None
        self.heartbeat = 0
        self.tickers = {}

        self.stat = StatModule()

        self.lock = RLock()

        self.wsc = WebSocketApp(self.WEB_SOCKET_URL,
                                on_open=lambda ws: self.on_open(ws),
                                on_close=lambda ws: self.on_close(ws),
                                on_message=lambda ws, msg: self.on_message(ws, msg),
                                on_error=lambda ws, err: self.on_error(ws, err)
                                )
        self.t_connection = None
        logger.debug("PoloniexWebSocketAPI object created")

    def reset(self):
        self.close()
        self.error = None
        self.heartbeat = time.time() + self.WARM_UP_TIME
        self.tickers = {}
        self.stat.reset()

    def open(self):
        logger.warning("Opening poloniex connection...")
        self.reset()

        # Запускаем поток websocket соеднинения. Этот поток попадает в обработчики "on_..."
        self.t_connection = Thread(target=self.__connection_controller, daemon=True)
        self.t_connection.start()

        self.ticker_controller_start()

    def close(self):
        self.ticker_controller_stop()

        if self.wsc.sock:
            logger.warning("Closing poloniex connection...")
            self.wsc.close()
            self.t_connection.join()

    def send_command(self, message):
        try:
            self.wsc.send(message)
        except Exception as e:
            print(e)

    def __connection_controller(self):
        logger.debug("Start __connection thread")
        self.wsc.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})

    @handle_api_exception
    def on_message(self, ws, message):
        """Метод вызывается при получении и для обработки сообщения"""
        self.heartbeat = time.time()
        tid, sequence_num, messages = self.task_control(message)

        if tid not in (1000, 1002, 1003, 1010):
            if sequence_num is not None:
                try:
                    self.tickers[tid].update(sequence_num, messages)
                except KeyError:
                    logger.warning("Pair with tid {0} does not exists".format(tid))

        else:
            # специальны набор кодов, для них нет обработки
            logger.debug("Code without handling: %s" % tid)

    def on_open(self, ws):
        """Метод вызывается при открытии соединения"""
        logger.warning("Poloniex connection opened")

    def on_close(self, ws):
        """Метод вызывается при закрыти соединения"""
        logger.warning("Poloniex connection closed")

    @handle_api_exception
    def on_error(self, ws, error):
        """Метод вызывается при возникновени ошибки"""
        logger.critical(error)
        raise WssConnectionError(error)

    def get_stat(self, key):
        return self.stat.get_stat(key)

    @check_state
    def get_data(self):
        trades, orderbooks, orderbook_volumes = [], [], []
        ts = int(time.time())
        for ticker in self.tickers.values():
            try:
                ticker_trades, ticker_orderbook, ticker_orderbook_volumes = ticker.get_data(ts)
            except PairNotInitialized:
                pass
            else:
                trades = trades + ticker_trades
                orderbooks = orderbooks + ticker_orderbook
                orderbook_volumes = orderbook_volumes + ticker_orderbook_volumes

        #self.stat.add_stat("trades_count", len(trades))
        #self.stat.add_stat("orderbooks_count", len(orderbooks))
        #self.stat.add_stat("orderbook_volumes_count", len(orderbook_volumes))
        return trades, orderbooks, orderbook_volumes
