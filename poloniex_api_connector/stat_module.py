class StatModule:
    def __init__(self):
        self.stat = {}

    def add_stat(self, key, val):
        if key not in self.stat:
            self.stat[key] = val
        else:
            self.stat[key] += val

    def get_stat(self, key, reset_key=True):
        try:
            result = self.stat[key]
            if reset_key:
                del self.stat[key]
        except KeyError:
            result = -1
        return result

    def reset(self):
        self.stat = {}

