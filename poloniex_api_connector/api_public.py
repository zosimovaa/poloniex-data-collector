import requests
import logging

logger = logging.getLogger(__name__)


class PublicAPIError(Exception):
    """Ошибка при обращении к публичному API"""
    CODES = (
        "PUBLIC_API_ERROR:REQUEST_ERROR"
        "PUBLIC_API_ERROR:UNKNOWN_COMMAND"
    )

    def __init__(self, err_code, method, body=None):
        self.err_code = err_code
        self.method = method
        self.body = body

    def __str__(self):
        message = "Code: {0}. Method: {1}".format(self.err_code, self.method)
        if self.body is not None:
            message += ". Response: {0})".format(self.body)
        return message


class PoloniexPublicAPI:
    """Класс реализует работу с публичным API poloniex.com"""
    PUBLIC_API_URL = "https://poloniex.com/public?command="
    METHODS = [
        "returnTicker",
        "return24hVolume",
        #"returnOrderBook",
        #"returnTradeHistory",
        #"returnChartData",
        #"returnCurrencies",
        #"returnLoanOrders"
    ]

    def execute(self, command):
        # TODO: Добавить обработку параметров команды
        if command in self.METHODS:
            try:
                response_raw = requests.get(self.PUBLIC_API_URL + command)
                response = response_raw.json()
            except Exception as e:
                raise PublicAPIError("PUBLIC_API_ERROR:REQUEST_ERROR", command, body=e)
        else:
            raise PublicAPIError("PUBLIC_API_ERROR:UNKNOWN_COMMAND", command)
        return response
