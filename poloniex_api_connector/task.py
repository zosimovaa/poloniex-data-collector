# Базовй класс для объектов, которые могут отправлять задания в очередь
# Реализует основные команды - subscribe, unsubscribe
# При переполнении очереди исключение Full будет выброшено в вызывающий процесс
#
#
# =============================================================================

import logging

# logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TaskEmitter:
    SUBSCRIBE = "subscribe"
    UNSUBSCRIBE = "unsubscribe"

    def __init__(self, queue=None, tid=None):
        self.queue = queue
        self.tid = tid
        self.init_number = 0
        logger.debug("TaskEmitter initialized for tid {}".format(self.tid))

    def subscribe(self, tid=None):
        if self.tid is not None:
            self.init_number += 1
            tid = self.tid

        logger.debug("Subscribe task added for ticker id {}".format(tid))
        task = {
            "tid": tid,
            "cmd": self.SUBSCRIBE,
            "attempt": 0
        }
        self.put_task(task)

    def unsubscribe(self, tid=None):
        if self.tid is not None:
            tid = self.tid
        logger.debug("Unsubscribe task added for ticker id {}".format(tid))
        task = {
            "tid": tid,
            "cmd": self.UNSUBSCRIBE,
            "attempt": 0}

        self.put_task(task)

    def put_task(self, task):
        self.queue.put_nowait(task)
