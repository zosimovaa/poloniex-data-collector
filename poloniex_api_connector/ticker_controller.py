import numpy as np
import logging
import time
from .api_public import PoloniexPublicAPI, PublicAPIError
from .ticker import Ticker, TickerStates, PairNotInitialized

from threading import Thread, RLock

logger = logging.getLogger(__name__)


class TickerController:
    WATCHDOG_TIMEOUT = 60

    def __init__(self, queue):
        self.queue = queue
        self.tickers = {}
        self.watchdog_run = True
        self.public_api = PoloniexPublicAPI()
        self.t_ticker_ctrl = None

    def ticker_controller_start(self):
        self.watchdog_run = True
        self.t_ticker_ctrl = Thread(target=self.ticker_controller, daemon=True)
        self.t_ticker_ctrl.start()

    def ticker_controller_stop(self):
        self.watchdog_run = False
        if self.t_ticker_ctrl is not None:
            self.t_ticker_ctrl.join()

    def ticker_controller(self):
        logger.warning("Ticker controller watchdog is running")
        while self.watchdog_run:
            try:
                self.__update_tickers()
                self.__update_tasks()
            except Exception as e:
                logger.error(e)
            finally:
                time.sleep(self.WATCHDOG_TIMEOUT)
        logger.warning("Ticker controller watchdog is stopped")

    def __update_tickers(self):
        # Обновление тикеров
        try:
            tickers_resp = self.public_api.execute("returnTicker")
        except PublicAPIError as e:
            logger.error(e)

        else:
            ticker_ids = [tickers_resp[key]["id"] for key in tickers_resp]

            # tickers to delete
            ticker_ids_to_delete = np.setdiff1d(list(self.tickers.keys()), ticker_ids)
            for ticker_id in ticker_ids_to_delete:
                logger.warning("Ticker controller - delete ticker: {}".format(ticker_id))
                del self.tickers[ticker_id]

            # tickers to add
            ticker_ids_to_add = np.setdiff1d(ticker_ids, list(self.tickers.keys()))
            for ticker_id in ticker_ids_to_add:
                logger.warning("Ticker controller - add ticker: {}".format(ticker_id))
                self.tickers[ticker_id] = Ticker(ticker_id, self.queue)

    def __update_tasks(self):
        for ticker in self.tickers.keys():
            if self.tickers[ticker].state == TickerStates.DEL:
                logger.warning("Ticker controller - delete bad ticker: {}".format(ticker))
                del self.tickers[ticker]
