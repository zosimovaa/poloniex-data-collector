import time
import logging
import numpy as np
from threading import RLock
from queue import Full
from .task import TaskEmitter

logger = logging.getLogger(__name__)


class PoloniexTickerError(Exception):
    """Базовый класс исключения для пары. Предполагает логирование ошибки и отправку в телегу"""
    def __init__(self, ticker, message=None):
        super().__init__(message)
        self.ticker = ticker
        self.message = message

    def __str__(self):
        msg = "Ticker {0} error".format(self.ticker)
        if self.message is not None:
            msg += " - {0}".format(self.message)
        return msg


class BadSequenceError(PoloniexTickerError):
    """Ошибка в торговой паре. Лог и переподписка"""
    def __str__(self):
        return "Ticker {0} bad sequence".format(self.ticker)


class PairNotInitialized(PoloniexTickerError):

    def __str__(self):
        return "Ticker {0} not initialized yet".format(self.ticker)


def check_state(func):
    def wrapper(ticker, *args, **kwargs):
        with ticker.lock:
            try:
                if ticker.state != TickerStates.INIT:
                    logger.debug("Ticker {0} init status is not True".format(ticker.name))
                    raise PairNotInitialized(ticker.name)
                elif not len(ticker.orderbook[0].keys()) or not len(ticker.orderbook[0].keys()):
                    logger.debug("Ticker {0} orderbook bad format".format(ticker.name))
                    raise PairNotInitialized(ticker.name)
            except IndexError:
                logger.debug("Ticker {0} orderbook is not initialized".format(ticker.name))
                raise PairNotInitialized(ticker.name)
        result = func(ticker, *args, **kwargs)
        return result
    return wrapper


def handle_ticker_exception(func):
    def wrapper(ticker, *args, **kwargs):
        result = None
        try:
            result = func(ticker, *args, **kwargs)
        except (PairNotInitialized, BadSequenceError) as e:
            logger.warning(e)
            with ticker.lock:
                if ticker.state == TickerStates.INIT:
                    logger.warning("Resubscribe pair {}".format(ticker.name))
                    ticker.reset()
                    #  Реализовать переподписку в процессе watchdog, а так же удаление пар, которые уже все
                    ticker.unsubscribe()

                    if ticker.init_number <= ticker.MAX_INIT_TIMES:
                        ticker.subscribe()
                    else:
                        ticker.state = TickerStates.DEL

        except Full:
            logger.error("The tasks queue is full! Ticker task didn't added - {0}".format(args))
            raise Full

        except PoloniexTickerError as e:
            logger.warning(e)

        except Exception as e:
            logger.error(e)
            raise Exception("Unexpected ticker {} exception".format(ticker.name)) from e
        return result
    return wrapper


class TickerStates:
    NOT_INIT = 'new ticker'                 # не инициализирован
    INIT = 'ticker initialized'             # инициализирован, ок
    DEL = 'ticker marked for delete'        # на удаление


class Ticker(TaskEmitter):
    LEVELS = [0.01, 0.02, 0.03, 0.05, 0.08, 0.13]
    MAX_INIT_TIMES = 3

    def __init__(self, tid, queue):
        TaskEmitter.__init__(self, tid=tid, queue=queue)
        self.name = tid

        self.lock = RLock()
        self.state = TickerStates.NOT_INIT
        self.sequence = 0

        self.orderbook = []
        self.trades = []
        self.orderbook_volumes = []

        # сразу подписываемся на обновления
        self.subscribe()
        logger.debug("Ticker %s: created" % self.name)

    @check_state
    def get_prices(self):
        lowest_ask = np.min(np.array(list(map(float, self.orderbook[0].keys()))))
        highest_bid = np.max(np.array(list(map(float, self.orderbook[1].keys()))))
        return lowest_ask, highest_bid

    @handle_ticker_exception
    def update(self, sequence, messages):
        with self.lock:
            # check sequence num of message
            self.__update_seq(sequence)
            if messages is not None:
                for message in messages:
                    content_type = ord(message.pop(0))     # почему-то были проблемы с обработкой "i", "o", "t".

                    # "i" - initialize message
                    if content_type == 105:
                        self.__init_message_handler(sequence, message)

                    # "o" - orderbook
                    elif content_type == 111:
                        self.__orderbook_message_handler(message)

                    # "t" - trade
                    elif content_type == 116:
                        self.__trade_message_handler(message)

                    else:
                        logger.warning(message)
                        raise PoloniexTickerError(self.name, "Bad message")

    def __update_seq(self, sequence_num):
        if self.state == TickerStates.INIT:
            if sequence_num == (self.sequence + 1):
                self.sequence = sequence_num
            else:
                raise BadSequenceError(self.name)

    def __init_message_handler(self, sequence, message):
        self.state = TickerStates.INIT
        self.sequence = sequence
        self.orderbook = message[0]["orderBook"]
        self.name = message[0]["currencyPair"]
        logger.warning("Pair {0} is initialized".format(self.name))

    def get_group(self, price, market_price):
        group = len(self.LEVELS)
        coef = np.abs(float(price) - market_price) / market_price
        for i in range(len(self.LEVELS)):
            if coef <= self.LEVELS[i]:
                group = i
                break
        return str(group)

    @handle_ticker_exception
    @check_state
    def __orderbook_message_handler(self, message):
        side, price, volume, ts = message

        # 1. Учет информации о входящем/исходящем объеме стакана
        volume_diff = float(volume) - float(self.orderbook[side].get(price, 0))
        prices = self.get_prices()
        market_price = prices[side]
        group = self.get_group(price, market_price)

        record = (self.name, int(ts)//1000, side, float(price), volume_diff, market_price, group)
        self.orderbook_volumes.append(record)

        # 2. Обновление текущего стакана
        self.orderbook[side][price] = volume

        # 3. Удаление нулевых записей из стакана
        if float(self.orderbook[side][price]) == 0.:
            del self.orderbook[side][price]

    @handle_ticker_exception
    @check_state
    def __trade_message_handler(self, message):
        # if side is 0 - this is a buy transaction from bid market (1)
        # if side is 1 - this is a sell transaction from ask market (0)
        trade_id, side, price, volume, timestamp, ts = message
        record = [self.name, timestamp, side, int(trade_id), price, volume]
        self.trades.append(record)

    @check_state
    def get_data(self,  ts):
        # 0 - ask - price from sellers
        # 1 - bid - price from buyers
        with self.lock:
            trades, self.trades = self.trades, []
            orderbook_volumes, self.orderbook_volumes = self.orderbook_volumes, []
            orderbook = []

            keys_asks = np.array(list(map(float, self.orderbook[0].keys())))
            vols_asks = np.array(list(map(float, self.orderbook[0].values())))
            keys_bids = np.array(list(map(float, self.orderbook[1].keys())))
            vols_bids = np.array(list(map(float, self.orderbook[1].values())))

        if len(keys_asks):
            lowest_ask, highest_bid = self.get_prices()
            asks, bids = [], []

            for coef in self.LEVELS:
                mask_ask = keys_asks < lowest_ask * (1 + coef)
                asks.append(sum(vols_asks[mask_ask]))

                mask_bid = keys_bids > highest_bid * (1 - coef)
                bids.append(sum(vols_bids[mask_bid]))

            record = tuple([self.name, ts, lowest_ask, highest_bid, tuple(asks), tuple(bids)])
            orderbook.append(record)
        return trades, orderbook, orderbook_volumes

    def reset(self):
        self.state = TickerStates.NOT_INIT
        self.orderbook = []
        self.orderbook_volumes = []
        self.sequence = 0
