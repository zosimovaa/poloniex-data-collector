# Проработка БД.

## Тезисы:
1. Запрос должен исполняться быстро на любой глубине (месяц+)
2. Запрос не должен быть сложным
3. Все данные должны быть получены в одном запросе (данные по всем парам точно, по периодам - вопрос)
-----------------------------------------------------------------------------

## Типы данных
 - состояние стаканов на моменты времени t0, t0-n, t0-2n, ... t0-kn
 - агрегация объемов торгов по k периодам размера n
-----------------------------------------------------------------------------

## Параметры выгрузки
Выгрузка будет производиться по разным интервалам. Количество запрашиваемых точек - допустим 30.
Интервалы:
	- 1 минута (30 минут)
	- 5 минут  (2.5 часа) 
	- 15 минут (7.5 часов)
	- 30 минут (15 часов)
	- 1 час    (30 часов)
	- 2 часа   (2.5 дня)
	- 8 часов  (10 дней)
	- 12 часов (15 дней)
	- 24 часа  (1 месяц)

-----------------------------------------------------------------------------

## Проработка

1. Пишем с некоторой периодичностью, определять метку по ближайшему timestamp
	- Записываем данные 2-3 раза в минуту с фактическим таймстемпом
	- Делаем выборку
		- округляем время
		- группируем по округленному времени, но берем с максимумом
		- делаем join, получаем на выходе ближайшее по времени значение

Если пропали данные - в БД не будет значений. Это надо будет фиксить на уровне приложения.
Т.к. я пишу все подряд, то отсутствие данных говорит о том, чтоприложение не раотало и период 

Для больших периодов надо фиксировать средние показатели. 
Может быть потом перейти к взвешенным средним


2. Как в пункте 1, только используем ReplacingMergeTree
Это позволит оставить только одно значение (по факту данных будет в 2-3 раза меньше)

Проблему с дублирующими записями решит запрос из пункта 1. По итогу индекс будет короче, данных меньше. 


Плюсы:  красивое и точное решение
Минусы: хз как выгрести записи по всем парам!

Если мы используем ReplacingMergeTree, то можно писать часто. Ставить timestamp в app, округленный до минуты.
Движок во время OPTIMIZE оставит только одну запись.

3. На уровне приложения фиксировать timestamp
	- Вычисляем время до следующей записи
	- Засыпаем на это врем
	- просыпаемся в момент записи
	- Фисируем timestamp
	- Собираем данные
	- Записываем в таблицу
	- Повторяем

Усложняется логика приложения в части записи


## Итого

1. Используем движок ReplacingMergeTree

2. Для истории торгов
	- контроллируем глубину записи при дозагрузке после сбоя
	- после дозагрузки вызываем OPTIMIZE

3. Для стаканов
	- Сохраняем все в плоской структуре, все данные в одной строке
	- Пишем 2-3 раза в минуту, движок сожмет данные. 

4. Выгрузка данных становиться простой
	- торги: просто агрегируем по периодам
	- стаканы: используем выгрузку с агрегацией, чтобы использовать актуальные значения (а не будет это тормозить???)



## Вопросы
1. Что делать при потере данных?
	- Торги: можем дозагрузить
	- Стаканы: использовать предыдыщеи значение (???) (усложнит выборку и не супер корректно)
