import time
import logging
from clickhouse_driver import connect

logger = logging.getLogger(__name__)

def with_debug_time(func):
    def wrapper(*args, **kwargs):
        t0 = time.time()
        result = func(*args, **kwargs)
        t1 = time.time()
        logger.warning("Exec time {0}: {1:.3} | num of records: {2}".format(func.__name__, t1-t0, result))
        return result
    return wrapper

class DBConnector(object):
    def __init__(self, params):
        self.params = params
        self.conn = None
        self.cursor = None

    def create_connection(self):
        return connect(**self.params)
    
    # @with_debug_time
    def write_data(self, query, data):
        if len(data):
            self.cursor.executemany(query, data)
            data_length = self.cursor.rowcount
        else:
            data_length = 0
        return data_length

    def __enter__(self):
        self.conn = self.create_connection()
        self.cursor = self.conn.cursor()
        logger.warning("Cursor created, database connection established")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.close()
