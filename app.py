import time
import traceback

from poloniex_api_connector import PoloniexWebSocketAPI
from poloniex_api_connector import PoloniexError, HeartbeatLostError

from db_connector import DBConnector

from log import create_logger
from cfg import read_config

APP_VER = "2.1.6"

if __name__ == "__main__":
    config = read_config()

    logger = create_logger(**config["logger"], **config["telegram"], name=config["common"]["name"])
    logger.critical("Запуск приложения. Версия: %s" % APP_VER)

    while True:
        try:
            logger.critical("Старт сессии")
            with DBConnector(config["db"]) as db:
                poloniex = PoloniexWebSocketAPI()
                poloniex.open()
                while True:
                    trades, orderbook, volumes = poloniex.get_data()

                    written_trades = db.write_data(config["queries"]["trades"], trades)
                    written_orderbook = db.write_data(config["queries"]["orderbook"], orderbook)
                    written_volumes = db.write_data(config["queries"]["volumes"], volumes)

                    logger.warning("Written data - trades: {0}, orderbooks: {1}, volumes: {2}".format(
                        written_trades, written_orderbook, written_volumes
                    ))

                    time.sleep(int(config["common"]["data_update_timeout"]))

        except (HeartbeatLostError, Exception) as e:
            logger.critical(e)
            logger.error(traceback.format_exc())
        finally:
            time.sleep(1)
