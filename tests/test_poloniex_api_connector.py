from poloniex_api_connector import Ticker
import copy
import unittest
from queue import Queue, Full

tasks = Queue(10)
tid = 1
pair = "USDT_BTC"


#--------------------------
#test data

heartbeat = [1010]
msg_i   = ["i", {"currencyPair": "BTC_ETH","orderBook":[{"1.05":"0.98", "1.10":"2.3"},{"0.95":"0.77", "0.90":"0.1"}]}, "1522877119341"]
msg_o_0 = ["o", 0, "1.15","0.5", "1522877119341"]
msg_o_1 = ["o", 1, "0.9", "55.3", "1522877119341"]
msg_o_2 = ["o", 0, "1.05", "0.", "1522877119341"]
msg_o_3 = ["o", 1, "0.95", "0.33", "1522877119341"]
msg_t_1 = ["t", "1", 0, "1.05", "0.08", 1522877119, "1522877119341"]
msg_t_2 = ["t", "1", 1, "0.95", "0.08", 1522877119, "1522877119341"]
msg_t_3 = ["t", "1", 0, "1.25", "0.08", 1522877119, "1522877119341"]

msg_u = ["u", "1", 0, "1.05", "0.08", 1522877119, "1522877119341"]




class TestTicker(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ticker = Ticker(pair, tid, tasks)

    def init_ticker(self, seq_num):
        self.ticker = Ticker(pair, tid, tasks)

        payload = [143, seq_num, [msg_i]]
        code, sequence_num, messages = copy.deepcopy(payload)
        self.ticker.update(sequence_num, messages)


    def test_initialize(self):
        # init - > initialized
        self.init_ticker(12101)
        self.assertEqual(self.ticker.orderbook, msg_i[1]["orderBook"])
        self.assertEqual(self.ticker.state, True)
        self.assertEqual(self.ticker.sequence, 12101)

    def test_bad_sequence(self):
        # bad sequence -> reqdy=False, have to initialize
        self.init_ticker(12101)

        payload = [143, 12104, [msg_i]]
        code, sequence_num, messages = copy.deepcopy(payload)
        self.ticker.update(sequence_num, messages)
        self.assertEqual(self.ticker.orderbook, [])
        self.assertEqual(self.ticker.state, False)
        self.assertEqual(self.ticker.sequence, 0)
        self.assertEqual(self.ticker.tasks.qsize(), 2)

    def test_unknown_message_type(self):
        # init - > initialized
        self.init_ticker(12101)

        # unknown message type -> nothing, message in log
        payload = [143, 12102, [msg_u]]
        code, sequence_num, messages = copy.deepcopy(payload)
        self.ticker.update(sequence_num, messages)
        self.assertEqual(self.ticker.orderbook, msg_i[1]["orderBook"])
        self.assertEqual(self.ticker.state, True)
        self.assertEqual(self.ticker.sequence, 12102)

    def test_not_initialized(self):
        payload = [143, 1210, [msg_o_0]]
        code, sequence_num, messages = copy.deepcopy(payload)
        self.ticker.update(sequence_num, messages)
        self.assertEqual(self.ticker.orderbook, [])
        self.assertEqual(self.ticker.state, False)
        self.assertEqual(self.ticker.sequence, 0)


    def test_ob_change(self):
        self.init_ticker(12101)
        payload = [143, 12102, [msg_o_0]]
        code, sequence_num, messages = copy.deepcopy(payload)
        self.ticker.update(sequence_num, messages)
        self.assertEqual(self.ticker.orderbook[0]["1.15"], "0.5")

        payload = [143, 12103, [msg_o_1]]
        code, sequence_num, messages = copy.deepcopy(payload)
        self.ticker.update(sequence_num, messages)
        self.assertEqual(self.ticker.orderbook[1]["0.9"], "55.3")

        payload = [143, 12104, [msg_o_2, msg_o_3]]
        code, sequence_num, messages = copy.deepcopy(payload)
        self.ticker.update(sequence_num, messages)
        self.assertEqual(self.ticker.orderbook[1]["0.95"], "0.33")

        with self.assertRaises(KeyError) as e:
           res = self.ticker.orderbook[0]["1.05"]

        state, orderbook = self.ticker.get_orderbook()
        self.assertEqual(state, True)




    def test_trades(self):
        self.init_ticker(12101)
        payload = [143, 12102, [msg_t_1]]
        code, sequence_num, messages = copy.deepcopy(payload)
        self.ticker.update(sequence_num, messages)
        self.assertEqual(self.ticker.trades[0][4], msg_t_1[3])
        self.assertEqual(self.ticker.trades[0][5], msg_t_1[4])

        payload = [143, 12103, [msg_t_2, msg_t_3]]
        code, sequence_num, messages = copy.deepcopy(payload)
        self.ticker.update(sequence_num, messages)
        self.assertEqual(self.ticker.trades[1][4], msg_t_2[3])
        self.assertEqual(self.ticker.trades[1][5], msg_t_2[4])
        self.assertEqual(self.ticker.trades[2][4], msg_t_3[3])
        self.assertEqual(self.ticker.trades[2][5], msg_t_3[4])

        state, trades = self.ticker.get_trades()
        self.assertEqual(len(trades), 3)
        self.assertEqual(state, True)

        self.assertEqual(len(self.ticker.trades), 0)

if __name__ == '__main__':
    unittest.main()