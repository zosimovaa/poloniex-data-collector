import os
import logging
from logging import StreamHandler
from logging.handlers import TimedRotatingFileHandler, RotatingFileHandler
from telegram import Bot
import collections
from datetime import datetime


class TelegramHandler(StreamHandler):

    SPAM_COUNT_STOP = 30
    SPAM_CONTROL_TIMEOUT = 300
    SPAM_BLOCK_TIMEOUT = 3600
    SPAM_MESSAGE = "Spam detected, pause"

    def __init__(self, token, chat_id, name):
        StreamHandler.__init__(self)
        self.bot = Bot(token=token)
        self.chat_id = chat_id
        self.name = name
        self.spam = False
        self.spam_buffer = collections.deque(maxlen=self.SPAM_COUNT_STOP)
        self.spam_last_block = datetime.fromtimestamp(0)

    def emit(self, msg):
        spam = self.__spam_control()
        if not spam:
            text = "{0}: {1}".format(self.name, self.format(msg))
            silent = self.__silent()
            try:
                self.bot.sendMessage(chat_id=self.chat_id, text=text, disable_notification=silent)
            except Exception as e:
                print("Something goes wrong - {}".format(e))

    def __silent(self):
        now = datetime.now()
        if (now.hour > 21) or (now.hour < 9):
            silent = True
        else:
            silent = False
        return silent

    def __spam_control(self):
        """The method detects spam messages (messages sent very often) and returns a flag indicating spam."""
        spam = False
        now = datetime.now()

        # Get current state based on block timeout
        last_block_timeout = now - self.spam_last_block
        if last_block_timeout.total_seconds() < self.SPAM_BLOCK_TIMEOUT:
            spam = True

        # Get current state based on time difference for last n events
        elif len(self.spam_buffer) == self.SPAM_COUNT_STOP:
            self.spam_buffer.append(now)
            delta = self.spam_buffer[-1] - self.spam_buffer[0]
            if delta.total_seconds() < self.SPAM_CONTROL_TIMEOUT:
                if not self.spam:
                    self.bot.sendMessage(chat_id=self.chat_id, text=self.SPAM_MESSAGE,
                                         disable_notification=True)
                spam = True
                self.spam_last_block = now

        # for non-init state (when buffer is not full yet)
        else:
            self.spam_buffer.append(now)

        self.spam = spam
        return spam


def create_logger(path=None, file=None, token=None, chat_id=None, level=30, name="Poloniex_dev"):

    full_path = os.path.join(path, file)

    if not os.path.exists(path):
        os.makedirs(path)

    formatter = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s: %(message)s')

    logger = logging.getLogger()
    logger.handlers = []
    logger.setLevel(int(level))

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    file_handler = RotatingFileHandler(full_path, maxBytes=500*1024, backupCount=15)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    telegram_handler = TelegramHandler(token, chat_id, name)
    telegram_handler.setLevel(logging.CRITICAL)
    logger.addHandler(telegram_handler)

    return logger
