import configparser


def read_config():
    app_config = {}

    config = configparser.RawConfigParser()
    try:
        with open("config.ini") as f:
            config.read_file(f)
    except IOError:
        print("===Run with test config===")
        with open("config.test.ini") as f:
            config.read_file(f)

    return config._sections

